from django.apps import AppConfig


class SimplelabwebsiteConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "SimpleLabWebsite"
