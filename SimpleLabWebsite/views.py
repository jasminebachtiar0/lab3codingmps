from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
# @param request : to capture all the request that are sent to our web


def index(request):
    return render(request, "index.html")


def check(request):
    number = request.GET.get("num", None)

    context = {
        "num": number,
        "modulo": int(number) % 2
    }

    return render(request, "check-number.html", context)


def generate(request, word):
    context = {
        "name": word
    }
    return render(request, "generate-word.html", context)
